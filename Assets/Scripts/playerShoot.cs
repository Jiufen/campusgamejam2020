﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerShoot : MonoBehaviour
{
    [SerializeField,Range(1f, 20)] private float throwForce;
    public GameObject rocasGameObject;
    public bool currentRock = false;
    void Start()
    {
        if(currentRock)
        {
            ApplicationModel.postionRocaActual = transform.localPosition;
            ApplicationModel.rotacionRocaActual = transform.localRotation;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if(ApplicationModel.startTimerUntilShoot){
            ApplicationModel.timerUntilShoot++;
        }
        if(currentRock)
        {
            gameObject.transform.localPosition = ApplicationModel.postionRocaActual;
            gameObject.transform.localRotation = ApplicationModel.rotacionRocaActual;
        }
        Shoot();
        setNextRockToCurrent();
    }
    void Shoot()
    {
        if(currentRock)
        {
            if (Input.GetButtonUp("Fire1"))
            {
                ApplicationModel.currentRocks--;
                gameObject.transform.parent = rocasGameObject.transform;
                currentRock=false;
                GetComponent<Rigidbody>().isKinematic = false;
                GetComponent<Rigidbody>().AddRelativeForce((new Vector3 ( 0.5f,0.2f,1) ) * throwForce * 100);
                ApplicationModel.startTimerUntilShoot = true;
            }
        }
    }
    void setNextRockToCurrent(){
        if(ApplicationModel.timerUntilShoot>30)
        {
            if(ApplicationModel.rocasPlayer.Count > 0)
            {
                GameObject rocaSiguiente = ApplicationModel.rocasPlayer.Dequeue();
                rocaSiguiente.GetComponent<playerShoot>().currentRock = true;
            }
            else{
                ApplicationModel.hasRockActual = false;
            }
            ApplicationModel.timerUntilShoot=0;
            ApplicationModel.startTimerUntilShoot=false;
        }
        
    }
    
}
