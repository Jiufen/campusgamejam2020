﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{

    private Vector3 initPosition;
    public Transform navDestination;
    private Vector3 destPosition;

    private NavMeshAgent navMeshAgent;


    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        initPosition = transform.position;
        destPosition = initPosition;
        StartCoroutine(SetRoute());
    }

    void GetDestPosition()
    {
        destPosition = destPosition == initPosition ? navDestination.position : initPosition;
    }

    IEnumerator SetRoute()
    {
        while (true) {
            if (navMeshAgent.remainingDistance == 0)
            {
                yield return new WaitForSeconds(3);
                GetDestPosition();
                navMeshAgent.destination = destPosition;

            }
            yield return new WaitForSeconds(1);
        }
    }
}
