﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rockPicked : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        if(ApplicationModel.currentRocks<10)
        {
            if(other.gameObject.tag == "Player"){
                ApplicationModel.currentRocks++;
                gameObject.GetComponent<Rigidbody>().isKinematic = true;
                gameObject.transform.parent = ApplicationModel.playerCamera;

                if(!ApplicationModel.hasRockActual){
                    ApplicationModel.hasRockActual = true;
                    gameObject.GetComponent<playerShoot>().currentRock = true;
                    gameObject.transform.localPosition = ApplicationModel.postionRocaActual;
                    gameObject.transform.localRotation = ApplicationModel.rotacionRocaActual;
                }
                else{      
                    ApplicationModel.rocasPlayer.Enqueue(this.gameObject);
                    gameObject.transform.position = ApplicationModel.playerCamera.position - ApplicationModel.playerCamera.forward;
                }   
            }
        }
        
    }
}
