﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationModel : MonoBehaviour
{
    
    static public Queue<GameObject> rocasPlayer = new Queue<GameObject>();
    static public Vector3 postionRocaActual;
    static public Quaternion rotacionRocaActual;
    static public bool hasRockActual = true;
    static public Transform playerCamera;
    static public bool startTimerUntilShoot = false;
    static public int timerUntilShoot=0;

    static public int currentRocks = 1;
    
}
