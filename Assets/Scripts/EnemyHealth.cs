﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnemyHealth : MonoBehaviour
{
    
    [SerializeField]private Transform player;
    [SerializeField]private Transform enemyLifeCanvas;
    [SerializeField]private Image lifeBar;

    private float enemyLife = 1;

    void Update()
    {
        enemyLifeCanvas.transform.LookAt(player);
        lifeBar.fillAmount = enemyLife;
    }

    private void OnCollisionEnter(Collision other) {
        if(other.gameObject.tag == "Rock"){
            enemyLife-= 0.2f;
        }
    }
}
